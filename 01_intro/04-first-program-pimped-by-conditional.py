"""This programm asks the user when the vacuum
   cleaner shall start cleaning and how long it
   should clean. Prints out and asks for confirmation.
"""

print("Dear user, in how many minutes shall I start cleaning?")

min_to_start = int(input())
""" Introduces conditions by evaluating user input.
    If the if statement is true the if clause will be executed.
    The if keyword indicates the beginning of a condition.
    Then comes the condiction itself, that needs to be evaluated.
    The colon (:) indicates the end of the if statement.
"""
if min_to_start < 0:
    # This is the if clause.
    # This one throws an exception so that the programm stops at this
    # position, the exception can be handled and the user (developer) gets
    # valuable information what went wrong.
    raise ValueError("You cannot let me start in the past!")
# If previous conditions are false, then this elif condition is evaluated.
elif min_to_start > 60 * 12:
    raise ValueError("You cannot let me start that late!")
# If previous conditions are false, then else is just executed.
else:
    print("Valid start value " + str(min_to_start) + " given!")

print("How many minutes shall I clean?")
min_cleaning = int(input())
print(
    "I will start in " + str(min_to_start) +
    " and end in " +
    str(min_to_start + min_cleaning) + " min"
)
