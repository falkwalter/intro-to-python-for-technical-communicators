"""Before we can control the flow of a program
we need to be able to compare. Using operators.
Best fire the following examples in the interactive
console"""

# Let"s start with equal to
"Hand-held" == "Backpack"
# outputs: False

"Hand-held" == "Hand-held"
42 == 6 * 7
# outputs: True

42 == "42"
# outputs: False


# contunue with not equal to
"Hand-held" != "Backpack"
# outputs: True

"Hand-held" != "Hand-held"
# outputs: False

42 != 6 * 7
# outputs: False

42 != "42"
# outputs: True

# comparing for lower or greater than
42 <= 43
# outputs: True

42 < 42.0
# outputs: False

# Not so helpful for strings
"Hand-held" < "Robotic"
# outputs: True

""" Important distinction:"""
# = assignes a value
my_value = 42

# == compares a value
my_value == 7
# outputs: False


""" Boolean operators """
# or: one True is enough!
True or False or False
# outputs: True

# and: all need to be True
True and False and False
# outputs: False

True and True and True
# outputs: True

# not: just reversing
not(True)
not(False)
