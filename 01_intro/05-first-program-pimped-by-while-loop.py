"""This programm asks the user when the vacuum
   cleaner shall start cleaning and how long it
   should clean. Prints out and asks for confirmation.
"""

print("Dear user, in how many minutes shall I start cleaning?")

min_to_start = int(input())
if min_to_start < 0:
    raise ValueError("You cannot let me start in the past!")
elif min_to_start > 60 * 12:
    raise ValueError("You cannot let me start that late!")
else:
    print("Valid start value " + str(min_to_start) + " given!")

print("How many minutes shall I clean?")
min_cleaning = int(input())
print(
    "I will start in " + str(min_to_start) +
    " and end in " +
    str(min_to_start + min_cleaning) + " min"
)

""" While loops are executed if and as long as their condition is true
    Very usefull also if we do not know yet how often we want to loops:
    - I might choose to change the waiting time until the vaccum cleaner
      starts!
    - We might have potentially never ending loops that shall only end if
      certain conditions are fulfilled. Example: A game.
"""
my_time = 0
while my_time < min_to_start:
    print(str(min_to_start - my_time) + " min until start!")
    my_time = my_time + 1
print("go!")
