"""This programm asks the user when the vacuum
   cleaner shall start cleaning and how long it
   should clean. Prints out and asks for confirmation.
"""

print("Dear user, in how many minutes shall I start cleaning?")

min_to_start = int(input())
if min_to_start < 0:
    raise ValueError("You cannot let me start in the past!")
elif min_to_start > 60 * 12:
    raise ValueError("You cannot let me start that late!")
else:
    print("Valid start value " + str(min_to_start) + " given!")

print("How many minutes shall I clean?")
min_cleaning = int(input())
print(
    "I will start in " + str(min_to_start) +
    " and end in " +
    str(min_to_start + min_cleaning) + " min"
)

my_time = 0
while my_time < min_to_start:
    print(str(min_to_start - my_time) + " min until start!")
    my_time = my_time + 1

""" for-loops iterate over a range of objects.
    So, integers must be converted into a range"""
for running_min in range(min_cleaning):
    print("Still " + str(min_cleaning - running_min) + " min running.")
    """ You can handover 3 arguments to range:
        - start
        - stop
        - step
        You can also nest conditions and loops, but beware of
        - readability
        - performance
    """
    for running_sec in range(1, 60, 30):
        print("brrr every 30 seconds")
