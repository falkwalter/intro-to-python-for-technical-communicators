""" Ready for your first program?"""
print("Hello World!")
# outputs "Hello World!"


""" Playing with data types
The following code will perform the same
as if you typed the numbers into a calculator,
may they
be free of decimal places (e.g. 12 => integer)"""
print(12 + 6 * 5)
# outputs "42"
"""or not (e.g. 5.5 => floating-point number):"""
print(5.5 ** 2)
# outputs "30.25"

""" Since we surely are not happy with using our
computer as calculator only let us mix in some
textual information (=> strings).
Gluing them together is done by a +.
Please take a short look on how the integer
value 3 can be used in combination with the texts:"""
print("There are " + "3" + " operation modes.")
# outputs "There are 3 operation modes."
print("There are " + str(6 / 2) + " operation modes.")
# outputs "There are 3.0 operation modes."
print("There are " + (3 * "operation modes."))
# outputs "There are operation modes.operation modes.operation modes."


""" Introducing Variables
Variables are great: You can stuff things inside them
and if you label it right you might find it later again"""
suction = 250
suction = suction**2
print(suction)
# outputs "62500"

configuration = "Hand-held"
configuration = str(suction) \
    + " Pa " \
    + configuration.lower() \
    + " vacuum cleaner"
print(configuration)
# outputs "62500 Pa hand-held vacuum cleaner"


"""One of the big differences of Python to other languages
is that you do not need to declare a type for variables
and the type can even change"""
print(suction)
# outputs "62500"
suction = "impressive"
print(suction)
# outputs "impressive"
