"""This programm asks the user when the vacuum
cleaner shall start cleaning and how long it
should clean. Prints out and asks for confirmation."""

print("Dear user, in how many minutes shall I start cleaning?")
min_to_start = int(input())

print("How many minutes shall I clean?")
min_cleaning = int(input())

print(
    "I will start in " + str(min_to_start) +
    " and end in " +
    str(min_to_start + min_cleaning) + " min"
)
