"""This programm asks the user when the vacuum
   cleaner shall start cleaning and how long it
   should clean. Prints out and asks for confirmation.
"""

"""Pulling the code into a dedicated function allows
   applying the DRY (Don"t repeat yourself) - known
   to TechWriters as reuse.
   So, we can implemement (test, maintain,...) code
   once but reuse it in several occurences.
   Also, you may document your functions using comments
   that can shown to the user (developer) while coding.
"""


def check_input(input_type, input_value):
    """Checks whether an input of a certain input type is valid.

    Parameters
    ------------
    input_type : str
        The type of input, so far "min_to_start" or "min_cleaning"
    input_value : int
        Value to be checked.

    Returns
    ------------
    str
        Confirmation message

    """

    upper_limit = 60 * 12
    if input_value < 0:
        raise ValueError("You cannot set " + input_type + " lower than 0!")
    elif input_value > upper_limit:
        raise ValueError("You cannot set " + input_type + " higher than " +
                         upper_limit + "!")
    else:
        return("Valid value " + str(input_value) + " for " + input_type +
               " given.")


print("Dear user, in how many minutes shall I start cleaning?")

min_to_start = int(input())
# First function call
print(check_input("min_to_start", min_to_start))

print("How many minutes shall I clean?")
min_cleaning = int(input())
# Second function call
print(check_input("min_cleaning", min_cleaning))
print(
    "I will start in " + str(min_to_start) +
    " and end in " +
    str(min_to_start + min_cleaning) + " min"
)

my_time = 0
while my_time < min_to_start:
    print(str(min_to_start - my_time) + " min until start!")
    my_time = my_time + 1

for running_min in range(min_cleaning):
    print("Still " + str(min_cleaning - running_min) + " min running.")
    for running_sec in range(1, 60, 30):
        print("brrr every 30 seconds")
