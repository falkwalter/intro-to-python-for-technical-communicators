"""This programm converts spreadsheets into a word document"""
from docx import Document
from docx.shared import Inches
from collections import OrderedDict
import openpyxl
import csv


def get_spreadsheet_data(file):
    """Gets data of spreadsheets file.

    Parameters
    ------------
    file : str
        path to spreadsheet file
        supports csv and MS Excel

    Returns
    ------------
    list with orderedDict
        content of the spreadsheet file, e.g.
        [OrderedDict([('part-no', '4711'),
                      ('name', 'body'),
                      ('pic', 'body.png')])]

    """

    if file[-3:] == "csv":
        with open(file) as my_csv_file:
            my_csv_reader = csv.DictReader(my_csv_file)
            return list(my_csv_reader)
    elif file[-4:] == "xlsx":
        my_excel_file = openpyxl.load_workbook(file)
        my_sheet = my_excel_file._sheets[0]
        max_col = my_sheet.max_column
        max_row = my_sheet.max_row

        my_excel_reader = []
        for my_row in range(2, max_row + 1):
            my_ordered_dict = OrderedDict()
            for my_col in range(1, max_col + 1):
                header = my_sheet.cell(row=1, column=my_col).value
                value = my_sheet.cell(row=my_row, column=my_col).value
                my_ordered_dict[header] = value
            my_excel_reader.append(my_ordered_dict)
        return (my_excel_reader)

    raise ValueError("File extension is not supported of file: " + file)


def write_word(file, content, template):
    """Writes a word file with a certain content

    Parameters
    ------------
    file : str
        path to word file
    content : list
        list of output. Inner list structure is type
        (para, img), style and content, e.g.
        (("para", "heading1", "Product Description"),
         ("para", "body", "The vacuum cleaner consists of the parts:")
         ("para", "list-bullet", "body (part no.: 4711)")
         ("table", "safety", {"risk":        "Dust explosion",
                              "level":       "Warning",
                              "description": "Sucking in too much dust...",
                              "mitigation":  "Fine dust such as plaster..."}))
    template : str
        path template file. Needs to contain styles in content.

    Returns
    ------------
    bool
        True if successfully written.

    """

    my_document = Document(template)
    for element in content:
        if element[0] == "para":
            my_document.add_paragraph(element[2], element[1])
        elif element[0] == "img-inline":
            my_document.add_picture(element[1], height=Inches(0.25))
        elif element[0] == "table" and element[1] == "safety":
            add_word_safety(my_document, element[2])
    my_document.save(file)
    return True


def add_word_safety(document, message):
    """Adds a safety message table to a document

    Parameters
    ------------
    document : Document
        docx Document
    message : dict
        dict describing the table to be written, e.g.
        {"risk":        "Dust explosion",
         "level":       "Warning",
         "description": "Sucking in too much dust...",
         "mitigation":  "Fine dust such as plaster..."}
    Returns
    ------------
    document
        Document with safety inserted table

    """
    table = document.add_table(1, 2)
    table.style = "safety"
    table.autofit = False
    table.cell(0, 0).width = Inches(1.5)
    table.cell(0, 0).paragraphs[0].add_run(text=message["level"])
    table.cell(0, 0).paragraphs[0].style = "safety-signal-word"
    safety_signal_para = table.cell(0, 0).add_paragraph("",
                                                        "safety-signal-sign")
    safety_signal_para.add_run().add_picture("test-data/pics/attention.png",
                                             width=Inches(1))
    table.cell(0, 1).width = Inches(5)
    table.cell(0, 1).paragraphs[0].add_run(text=message["risk"])
    table.cell(0, 1).paragraphs[0].style = "safety-risk"
    table.cell(0, 1).add_paragraph(message["description"],
                                   "safety-description")
    table.cell(0, 1).add_paragraph(message["mitigation"],
                                   "safety-mitigation")
    document.add_paragraph("", "body")


csv_file = "test-data/bom.csv"
excel_file = "test-data/hazard-analysis.xlsx"
template_file = "test-data/template.docx"
word_file = "test-data/02-excel-to-word.docx"

product_data = get_spreadsheet_data(csv_file)
content = []
content.append(("para", "heading1", "Product Description"))
content.append((("para", "body",
                 "The vacuum cleaner consists of the parts:")))
for part in product_data:
    content.append(("para",
                    "list-bullet",
                    part['name'] + " (part. no.: " +
                    part['part-no'] + ")"))
    content.append(("img-inline", "test-data/pics/" +
                    part['pic']))

safety_messages = get_spreadsheet_data(excel_file)
content.append(("para", "heading1", "Safety"))
for safety_message in safety_messages:
    content.append(("table",
                    "safety",
                    safety_message))

write_word(word_file, content, template_file)
print(word_file + " successfully created.")
