"""This programm converts spreadsheets and
   iirds data into a word document and
   spoken text"""
from docx import Document
from docx.shared import Inches
from collections import OrderedDict
from gtts import gTTS
from playsound import playsound
import xml.etree.ElementTree as ET
import openpyxl
import csv
import zipfile


def get_spreadsheet_data(file):
    """Gets data of spreadsheets file.

    Parameters
    ------------
    file : str
        path to spreadsheet file
        supports csv and MS Excel

    Returns
    ------------
    list with orderedDict
        content of the spreadsheet file, e.g.
        [OrderedDict([('part-no', '4711'),
                      ('name', 'body'),
                      ('pic', 'body.png')])]

    """

    if file[-3:] == "csv":
        with open(file) as my_csv_file:
            my_csv_reader = csv.DictReader(my_csv_file)
            return list(my_csv_reader)
    elif file[-4:] == "xlsx":
        my_excel_file = openpyxl.load_workbook(file)
        my_sheet = my_excel_file._sheets[0]
        max_col = my_sheet.max_column
        max_row = my_sheet.max_row

        my_excel_reader = []
        for my_row in range(2, max_row + 1):
            my_ordered_dict = OrderedDict()
            for my_col in range(1, max_col + 1):
                header = my_sheet.cell(row=1, column=my_col).value
                value = my_sheet.cell(row=my_row, column=my_col).value
                my_ordered_dict[header] = value
            my_excel_reader.append(my_ordered_dict)
        return (my_excel_reader)

    raise ValueError("File extension is not supported of file: " + file)


def write_word(file, content, template):
    """Writes a word file with a certain content

    Parameters
    ------------
    file : str
        path to word file
    content : list
        list of output. Inner list structure is type
        (para, img), style and content, e.g.
        (("para", "heading1", "Product Description"),
         ("para", "body", "The vacuum cleaner consists of the parts:")
         ("para", "list-bullet", "body (part no.: 4711)")
         ("table", "safety", {"risk":        "Dust explosion",
                              "level":       "Warning",
                              "description": "Sucking in too much dust...",
                              "mitigation":  "Fine dust such as plaster..."}))
    template : str
        path template file. Needs to contain styles in content.

    Returns
    ------------
    bool
        True if successfully written.

    """

    my_document = Document(template)
    for element in content:
        if element[0] == "para":
            my_document.add_paragraph(element[2], element[1])
        elif element[0] == "img-inline":
            my_document.add_picture(element[1], height=Inches(0.25))
        elif element[0] == "table" and element[1] == "safety":
            add_word_safety(my_document, element[2])
    my_document.save(file)
    return True


def add_word_safety(document, message):
    """Adds a safety message table to a document

    Parameters
    ------------
    document : Document
        docx Document
    message : dict
        dict describing the table to be written, e.g.
        {"risk":        "Dust explosion",
         "level":       "Warning",
         "description": "Sucking in too much dust...",
         "mitigation":  "Fine dust such as plaster..."}
    Returns
    ------------
    document
        Document with safety inserted table

    """
    table = document.add_table(1, 2)
    table.style = "safety"
    table.autofit = False
    table.cell(0, 0).width = Inches(1.5)
    table.cell(0, 0).paragraphs[0].add_run(text=message["level"])
    table.cell(0, 0).paragraphs[0].style = "safety-signal-word"
    safety_signal_para = table.cell(0, 0).add_paragraph("",
                                                        "safety-signal-sign")
    safety_signal_para.add_run().add_picture("test-data/pics/attention.png",
                                             width=Inches(1))
    table.cell(0, 1).width = Inches(5)
    table.cell(0, 1).paragraphs[0].add_run(text=message["risk"])
    table.cell(0, 1).paragraphs[0].style = "safety-risk"
    table.cell(0, 1).add_paragraph(message["description"],
                                   "safety-description")
    table.cell(0, 1).add_paragraph(message["mitigation"],
                                   "safety-mitigation")
    document.add_paragraph("", "body")


def speak(content):
    """Speaks certain content.
       Prints to stdout for better understanding.

    Parameters
    ------------
    content : list
        list of output. Inner list structure is type
        (para, img), style and content, e.g.
        (("para", "heading1", "Product Description"),
         ("para", "body", "The vacuum cleaner consists of the parts:")
         ("para", "list-bullet", "body (part no.: 4711)")
         ("table", "safety", {"risk":        "Dust explosion",
                              "level":       "Warning",
                              "description": "Sucking in too much dust...",
                              "mitigation":  "Fine dust such as plaster..."}))

    Returns
    ------------
    bool
        True if successfully spoken.

    """

    text = []
    sound_file = "test-data/spoken-documentation.mp3"
    speech_tags = {"heading1": "A section follows with the heading",
                   "body": "A normal paragraph follows.",
                   "list-bullet": "A bulleted list item follows.",
                   "safety": "A safety message follows!",
                   "level": "",
                   "risk": "You are running into the following risk:",
                   "description": "A description follows.",
                   "mitigation": "Mitigate the risk as follows.",
                   "img-inline": "Image follows with file name"}

    for element in content:
        if element[0] == "para":
            text.append(speech_tags[element[1]] + " " + element[2])
        elif element[0] == "img-inline":
            text.append(speech_tags["img-inline"] + " " + element[1] + ". ")
        elif element[0] == "table" and element[1] == "safety":
            text.append(speech_tags[element[1]] + " ")
            for subelement in element[2]:
                text.append(speech_tags[subelement] + " " +
                            element[2][subelement])
    text = " ".join(text)
    print(text)
    tts = gTTS(text=text, lang="en")
    tts.save(sound_file)
    playsound(sound_file)
    return True


def get_iirds(iirds_package, element):
    """Gets data of an iiRDS zip file.
    iiRDS - The International Standard for
    Intelligent Information Request and Delivery
    See https://iirds.org/ for more information.

    Parameters
    ------------
    iirds_package : str
        path to zip file container
        supports iiRDS version 1.0.1
        (only little slice for demo use)

    element : str
        information to be taken. For sake of
        simplicity we just support "safety"
        right now.

    Returns
    ------------
    list with orderedDict, e.g.
        [OrderedDict([('risk', 'Dangerous voltage: 230V'),
                      ('level', 'DANGER'),
                      ('description', 'Electric shocks can ...'),
                      ('mitigation', 'Have the work on ...')])]

    """
    source_dir = "test-data/iirds_data"
    iirds_zip_file = zipfile.ZipFile(iirds_package)
    iirds_zip_file.extractall(source_dir)

    metadata_tree = ET.parse(source_dir + "/META-INF/metadata.rdf")
    result = []

    if element == "safety":
        iirds_ns = "{http://iirds.tekom.de/iirds#}"
        xPath_exp = f".//{iirds_ns}Topic/"
        xPath_exp += f"{iirds_ns}title[.='Safety']/..//{iirds_ns}source"
        source_files = metadata_tree.findall(xPath_exp)
        for source_file in source_files:
            file = source_file.text
            result.extend(get_iirds_safety(source_dir + "/" + file))
    return result


def get_iirds_safety(file):
    """Gets safety messages of an example iiRDS topic file.
    iiRDS - The International Standard for
    Intelligent Information Request and Delivery
    See https://iirds.org/ for more information.

    Parameters
    ------------
    file : str
        file where safety messages to be extracted are.


    Returns
    ------------
    list with orderedDict, e.g.
        [OrderedDict([('risk', 'Dangerous voltage: 230V'),
                      ('level', 'DANGER'),
                      ('description', 'Electric shocks can ...'),
                      ('mitigation', 'Have the work on ...')])]
    """

    my_iirds_reader = []
    file_tree = ET.parse(file)
    xhtml_ns = "{http://www.w3.org/1999/xhtml}"
    xPath_exp = f".//{xhtml_ns}div[@class='safety-header']/.."
    xPath_exp_miti = f".//{xhtml_ns}ol[@class='schema-instruction']/"
    xPath_exp_miti += f"{xhtml_ns}li"
    m_exps = {"risk": f".//{xhtml_ns}div[@class='safety-cause']",
              "level": f".//{xhtml_ns}div[@class='safety-header']",
              "description": f".//{xhtml_ns}p[@class='safety-consequence']",
              "mitigation": xPath_exp_miti}
    safety_messages = file_tree.findall(xPath_exp)
    for safety_message in safety_messages:
        my_ordered_dict = OrderedDict()
        for m_exp in m_exps:
            try:
                value = safety_message.find(m_exps[m_exp]).text
            except AttributeError:
                value = ""
            my_ordered_dict[m_exp] = value
        my_iirds_reader.append(my_ordered_dict)
    return my_iirds_reader


csv_file = "test-data/bom.csv"
excel_file = "test-data/hazard-analysis.xlsx"
template_file = "test-data/template.docx"
word_file = "test-data/04-csv-excel-iirds-to-speech.docx"
iirds_package = "test-data/127466635_en.iirds"

product_data = get_spreadsheet_data(csv_file)
content = []
content.append(("para", "heading1", "Product Description"))
content.append((("para", "body", "The vacuum cleaner consists of the parts:")))
for part in product_data:
    content.append(("para",
                    "list-bullet",
                    part['name'] + " (part. no.: " +
                    part['part-no'] + ")"))
    content.append(("img-inline", "test-data/pics/" + part['pic']))

safety_messages = get_spreadsheet_data(excel_file)
content.append(("para", "heading1", "Safety"))
for safety_message in safety_messages:
    content.append(("table",
                    "safety",
                    safety_message))

supplier_data = get_iirds(iirds_package, "safety")
content.append(("para", "heading1", "Supplier Documentation"))
for safety_message in supplier_data:
    content.append(("table",
                    "safety",
                    safety_message))

write_word(word_file, content, template_file)
print(word_file + " successfully created.")
speak(content)
