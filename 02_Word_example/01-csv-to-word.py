"""This programm converts a bom into a word doc"""
from docx import Document
from docx.shared import Inches
import csv


def get_csv_data(file):
    """Gets and maps data of csv file.

    Parameters
    ------------
    file : str
        path to csv file

    Returns
    ------------
    list with orderedDict
        content of the csv file, e.g.
        [OrderedDict([('part-no', '4711'),
                      ('name', 'body'),
                      ('pic', 'body.png')])]

    """

    with open(file) as my_csv_file:
        my_csv_reader = csv.DictReader(my_csv_file)
        return list(my_csv_reader)


def write_word(file, content, template):
    """Writes a word file with a certain content

    Parameters
    ------------
    file : str
        path to word file
    content : list
        list of output. Inner list structure is type
        (para, img), style and content, e.g.
        (("para", "heading1", "Product Description"),
         ("para", "body", "The vacuum cleaner consists of the parts:")
         ("para": "list-bullet", "body (part no.: 4711)"))
    template : str
        path template file. Needs to contain styles in content.

    Returns
    ------------
    bool
        True if successfully written.

    """

    my_document = Document(template)
    for element in content:
        if element[0] == "para":
            my_document.add_paragraph(element[2], element[1])
        elif element[0] == "img-inline":
            my_document.add_picture(element[1], height=Inches(0.25))
    my_document.save(file)
    return True


csv_file = "test-data/bom.csv"
template_file = "test-data/template.docx"
word_file = "test-data/01-csv-to-word.docx"

product_data = get_csv_data(csv_file)
content = []
content.append(("para", "heading1",
                "Product Description"))
content.append((("para", "body",
                 "The vacuum cleaner consists of:")))
for part in product_data:
    content.append(("para",
                    "list-bullet",
                    part['name'] + " (part. no.: " +
                    part['part-no'] + ")"))
    content.append(("img-inline", "test-data/pics/" +
                    part['pic']))
write_word(word_file, content, template_file)
print(word_file + " successfully created.")
